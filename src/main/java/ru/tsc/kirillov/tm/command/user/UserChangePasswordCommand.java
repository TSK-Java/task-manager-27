package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "change-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля пользователя.";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getUserId();
        System.out.println("[Изменение пароля пользователя]");
        System.out.println("Введите новый пароль");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        getUserService().setPassword(userId, newPassword);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
