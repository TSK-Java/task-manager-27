package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectUnbindTaskByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-unbind-task-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отвязать задачу от проекта.";
    }

    @Override
    public void execute() {
        System.out.println("[Отвязка задачи от проекта]");
        System.out.println("Введите ID проекта:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskToProject(getUserId(), projectId, taskId);
    }

}
