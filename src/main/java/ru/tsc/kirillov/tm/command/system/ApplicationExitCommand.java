package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Закрытие приложения.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
