package ru.tsc.kirillov.tm.exception.system;

import ru.tsc.kirillov.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Ошибка! Доступ запрещён.");
    }

}
