package ru.tsc.kirillov.tm.exception.system;

import ru.tsc.kirillov.tm.exception.AbstractException;

public final class CommandNotRegisteredException extends AbstractException {

    public CommandNotRegisteredException() {
        super("Ошибка! Команда не может быть зарегистрирована.");
    }

}
